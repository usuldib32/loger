package pierwszy_log;

import java.time.LocalDate;

public class User {

    public int id;
    private String name;
    private String surname;
    private String login;
    private String password;
    private String email;
    private int birthDate;

    public User(int id, String name, String surname, String login, String password, String email, int birthDate) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.email = email;
        this.birthDate = birthDate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public String getLogIn(){
        return login;
    }

    public String getPassword() {
        return password;
    }
}
