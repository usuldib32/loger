package pierwszy_log;

import java.util.ArrayList;
import java.util.List;

public class UserServicImp implements UserService {
    private List<User> usersList = new ArrayList<>();

    @Override
    public boolean register(User user) {
        User checkUser;
        for (User element : usersList) {
            checkUser = element;
            if (checkUser.getLogIn().equals(user.getLogIn())) {
                System.out.println("Użytkownik istnieje.");
                return false;
            }
        }
        usersList.add(user);
        System.out.println("Użytkownik dodany.");
        return true;
    }


    @Override
    public boolean logIn(String login, String password) {
//    public boolean logIn(User user) {
        User checkUser;
        for (User element : usersList) {
            checkUser = element;
            if (checkUser.getLogIn().equals(login) && checkUser.getPassword().equals(password)) {
                System.out.println("Zostałeś zalogowany");
                return true;
            } else System.out.println("Niepoprawny uytkownik lub niepoprawne hasło.");
        }
        return false;
    }

    @Override
    public void changePass(String login, String oldPass, String newPass) {
        User checkUser;
        for (User element : usersList) {
            checkUser = element;
            if (checkUser.getLogIn().equals(login)) {
                if (checkUser.getPassword().equals(oldPass)) {
                    checkUser.setPassword(newPass);
                    System.out.println("Hasło zmienione");
                }
        } else System.out.println("Stare hasło niepoprawne.");

    }


}}
