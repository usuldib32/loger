package pierwszy_log;


import org.apache.log4j.Logger;

public class UserMain {
    protected final static Logger log = Logger.getLogger(UserMain.class.getName());

    public static void main(String[] args) {
log.info("Log o poziomie info");
        UserService userService = new UserServicImp();
        User user1 = new User(1, "User1", "Surname1", "UserLogin1", "password1", "user1", 10);
        User user2 = new User(1, "User2", "Surname2", "UserLogin2", "password2", "user2", 20);
        User user3 = new User(1, "User2", "Surname2", "UserLogin2", "password2", "user2", 20);
        userService.register(user1);
//        userService.register(user2);
//        userService.register(user3);
        userService.logIn("UserLogin1","password1");
//        userService.logIn(user2);
//        userService.logIn(user3);
        userService.changePass("UserLogin1", "password1", "newpassword1");
        log.warn("To jest log o poziomie warning");
    }
}
