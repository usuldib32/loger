package pierwszy_log;

public interface UserService {

    boolean register(User user);
//    boolean logIn(String login, String password);
    boolean logIn(String login, String password);
//    boolean logIn(User user);
    void changePass(String login, String oldPass, String newPass);

}
